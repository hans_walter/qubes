from datetime import datetime
import logging
import os
import random
import string
import subprocess

import lvm

class LoopDevice():
    """ This class is used for safely adding and removing loopback devices when
        finished working with them.  It returns a device string of the added
        loopback device. 

        A loopback device is needed for mounting a file image Linux losetup is
        used here. See also man 8 losetup 
    """


    logger = logging.getLogger(__name__)
    loop = None 
    path = None

    def __init__(self, path):
        self.path = path

    def __enter__(self):
        self.loop = execute(['losetup', '-f', '--show', self.path])
        return self.loop

    def __exit__(self, type, value, traceback):
        execute(['losetup', '-d', self.loop])


class MountDevice():
    """ This class is used for safely mount and unmount devices when finished
        working with them. It returns a path where the device is mounted. 
        
        GNU mount is used here. See also man 8 mount 
    """
    logger = logging.getLogger(__name__)

    device = None 
    mount_point = None
    def __init__(self, device):
        self.device = device


    def __enter__(self):
        self.logger.debug("Trying to create a mount point")

        # Make sure we do not accidentally create something that already
        # exists. Not sure how helpful this is

        self.mount_point = '/tmp/xuy/' + self.generate_name()
        while os.path.exists(self.mount_point):
            self.mount_point = '/tmp/xuy/' + self.generate_name()

        os.makedirs(self.mount_point)
        execute(['mount', self.device, self.mount_point])

        return self.mount_point

    def __exit__(self, type, value, traceback):
        execute(['umount', self.mount_point])
        os.rmdir(self.mount_point)

    def generate_name(self) :
        """ Generates a pseudorandom 6 lower case char + digits string. It is
            used as a random name for the mount point.

            Shamelesly ripped of from http://stackoverflow.com/a/2257449
        """


        return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(6))

class MountReadOnly(MountDevice):
    """ This class is used to mount a device read only 
        GNU mount is used here. See also man 8 mount 
    """

    device = None 
    mount_point = None
    def __init__(self, device):
        self.device = device

    logger = logging.getLogger(__name__)

    def __enter__(self):
        self.logger.debug("Trying to create a mount point")

        # Make sure we do not accidentally create something that already
        # exists. Not sure how helpful this is

        self.mount_point = '/tmp/xuy/' + self.generate_name()
        while os.path.exists(self.mount_point):
            self.mount_point = '/tmp/xuy/' + self.generate_name()

        os.makedirs(self.mount_point)
        execute(['mount', '-oro', self.device, self.mount_point])

        return self.mount_point

    def __exit__(self, type, value, traceback):
        execute(['umount', self.mount_point])

    def generate_name(self) :
        """ Generates a pseudorandom 6 lower case char + digits string. It is
            used as a random name for the mount point.

            Shamelesly ripped of from http://stackoverflow.com/a/2257449
        """


        return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(6))


logger = logging.getLogger(__name__)

class VolumeGroup():
    """
    Open and close a LVM2 Volume Group
    """
    logger = logging.getLogger(__name__)
    def __init__(self, vg_name='qubes_dom0', access='r'):
        self.vg_name = vg_name
        self.access = access

    def __enter__(self):
        self.logger.debug('Opening Volume Group: ' + self.vg_name)
        self.vg =  lvm.vgOpen(self.vg_name, self.access)
        return self.vg


    def __exit__(self, type, value, traceback):
        self.vg.close()



def lvcreate(name = None, size = 0, vg_name = None):
    if  name is None :
        logger.error('lvcreate name not provided')
        raise ValueError

    if  size <= 0 :
        logger.error('lvcreate size must be > 0')
        raise ValueError

    root_img_name = datetime.now().strftime(name + '_%Y%m%d%H%M%S')

    with VolumeGroup(vg_name=vg_name, access='w') as vg:
        lv = vg.createLvThin("xuy_pool", root_img_name, size)
        logger.info("Created %s", root_img_name)

    return root_img_name

def lvremove(name):
    vg = lvm.vgOpen('datengrab', 'w')
    vg.lvFromName(name).remove()
    vg.close()
    logger.info("Removed %s", name)


def execute(command):
    #command.insert(0, 'sudo')
    try:
        result = subprocess.check_output(command).strip()
        logger.debug('Executed ' + ' '.join(command))
        return result
    except subprocess.CalledProcessError as e:
        logger.error('Failed ' + ' '.join(command))
        raise e

def get_vm_list():
    """ Returns an initialized QubesVmCollection this just a handy shortcut
    """

    qvm = QubesVmCollection()
    qvm.lock_db_for_reading()
    qvm.load()
    qvm.unlock_db()
    return qvm

def get_qvm(name):
    """ Fetches vm by name. Just a shortcut
    """
    vms = get_vm_list()
    for k, vm in vms.items():
        if vm.name == name:
            return vm

def clone_file_img(path = None, name = None, vg_name='qubes_dom0', pool = 'pool0'):
    if name is None:
        log.error('No vm name provided')
        raise ValueError

    if path is None:
        log.error('No path provided')
        raise ValueError

    with LoopDevice(path) as loop:
        logger.debug("Added Loopback device")
        with MountReadOnly(loop) as origin_mount_point:
            logger.debug("Mounted Qubes image")
            root_img_name = lvcreate(name=name, size=os.path.getsize(path), vg_name=vg_name)
            import_device = '/dev/datengrab/' + root_img_name
            try:
                print execute(['mkfs.ext4', import_device ])
                with MountDevice(import_device) as import_mount_point:
                    print execute(['rsync', '-a', origin_mount_point + '/', import_mount_point])
                    return root_img_name
            except subprocess.CalledProcessError as e:
                lvremove(root_img_name)
                logger.error('Failed to import %s', name)
                raise e

def mkswap(name = None, vg='qubes_dom0', pool='pool0', size=2147483648): #2GB swap per default
    if name is None:
        log.error('No vm name provided')
        raise ValueError


    name = datetime.now().strftime('swap_'+ name + '_%Y%m%d%H%M%S')
    with VolumeGroup(vg_name=vg_name, access='w') as vg:
        lv = vg.createLvThin("xuy_pool", name, size)

