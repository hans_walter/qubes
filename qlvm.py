import logging

from qubes.qubes import QubesVm
from qubes.qubes import defaults,system_path,dry_run

import devices

class QubesTemplateNG(QubesVm):
    """
    A class that represents a TemplateVm which uses a Logical Volume
    Manager for it's disks
    """

    load_order = 40
    logger = logging.getLogger(__name__)

    def get_attrs_config(self):
        attrs_config = super(QubesTemplateNG, self).get_attrs_config()
        attrs_config['dir_path']['func'] = \
            lambda value: value if value is not None else \
                os.path.join(system_path["qubes_templates_dir"], self.name)
        attrs_config['label']['default'] = defaults["template_label"]

        attrs_config['storage'] = { 'default':  'lvm'}
        attrs_config['vg_name'] = { 'default': 'datengrab'}
        attrs_config['pool'] = {'xuy_pool' }
        #self.logger.debug(attrs_config)
        return attrs_config


    @property
    def type(self):
        return "TemplateNG"

    @property
    def updateable(self):
        return True

    def is_template(self):
        return True

    def get_rootdev(self, source_template=None):
        if self.template:
            return "'phy:file//:{dev},xvda,w',".format(dev=self.root_img)

    def create_on_disk(self, verbose = False, source_template = None, vg_name='qubes_dom0', pool='pool0'):
        logger = self.logger
        if verbose:
            logging.basicConfig(level=logging.DEBUG)

        if source_template is None:
            source_template = self.template
        assert source_template is not None

        if dry_run:
            return True

        logger.debug("Creating the VM storage")
        self.create_storage(source_template)


        logger.debug("Creating the VM directory %s", self.dir_path)
        os.mkdir (self.dir_path)


        self.logger.debug("Creating the VM config file: {0}".format(self.conf_file))
        self.create_config_file(source_template = source_template)

        kernels_dir = source_template.kernels_dir
        logger.debug("Copying the kernel (set kernel \"none\" to use it): {0}". format(kernels_dir))

        os.mkdir (self.dir_path + '/kernels')
        for f in ("vmlinuz", "initramfs", "modules.img"):
            shutil.copy(os.path.join(kernels_dir, f),
                    os.path.join(self.dir_path, vm_files["kernels_subdir"], f))
        logger.debug("Creating icon symlink: {0} -> {1}".format(self.icon_path, self.label.icon_path))
        os.symlink (self.label.icon_path, self.icon_path)

        # fire hooks
        for hook in self.hooks_create_on_disk:
            hook(self, verbose, source_template=source_template)

        return True

    def create_storage(self, source_template = None):
        if not source_template.is_template():
            logger.error(source_template.name + " is not a valid template")

        if source_template is None:
            source_template = self.template
        assert source_template is not None

        if source_template.type is 'TemplateVM':
            if self.storage is 'lvm':
                self.hooks_get_config_params['rootdev'] = devices.clone_file_img(
                        path=source_template.root_img, name = self.name + '_root_',
                        vg_name=self.vg_name, pool=self.pool)
                self.hooks_get_config_params['privatedev'] = devices.clone_file_img(
                            path = source_template.private_img, name =self.name + '_home_',
                            vg_name=self.vg_name, pool=self.pool)
                self.hooks_get_config_params['volatiledev'] = None
                #lvmtools.mkswap(
                        #name = self.name, vg_name = self.vg_name,
                        #pool = self.pool
                        #)

            elif self.storage is 'zfs':
                logger.error('TODO: Implement copy file template to zfs volume')
        elif source_template.type is 'TemplateNG' and source_template.backend is not self.backend:
            logger.error('TODO: Implement copy zfs volume to lvm and vice versa')

        elif source_template.type is 'TemplateNG' and source_template.backend is self.backend:
            if self.storage is 'lvm':
                logger.error('TODO: Implement lvm snapshot')
            elif self.storage is 'zfs':
                logger.error('TODO: Implement zfs snapshot')
        else:
            logger.error("Uknown template type")


    def create_config_file(self, file_path = None, source_template = None, prepare_dvm = False):
        None
