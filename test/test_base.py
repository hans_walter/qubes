import os
import random
import string
import sys
import unittest
import logging

sys.path.insert(0, os.path.dirname(__file__) + '/../')

from qubes.qubes import register_qubes_vm_class
from qubes.qubes import QubesVmCollection
from qlvm import QubesTemplateNG



class TestBase(unittest.TestCase):

    def setUp(self):
        self._initLogger()
        self.qvm = QubesVmCollection()
        register_qubes_vm_class(QubesTemplateNG)
        self.name = ''
        for _ in range(6): self.name += random.choice(string.lowercase)
        self.vm = self.qvm.add_new_vm('QubesTemplateNG', name=self.name)
        self.vg_name = 'datengrab'
        self.pool = 'xuy_pool'


    def _cod(self, **kwargs):
        """
            create_on_disk shortcut
        """

        return  self.vm.create_on_disk(**kwargs)

    def _initLogger(self):
        # See http://tinyurl.com/mnkvhop
        self.logger = logging.getLogger()
        self.logger.level = logging.DEBUG
        self.logger.addHandler(logging.StreamHandler(sys.stdout))


class Lock:
    def __init__(self, qvm):
        self.qvm = qvm

    def __enter__(self):
        self.qvm_collection  = QubesVmCollection()
        self.qvm_collection .lock_db_for_writing()
        self.qvm_collection .load()
        return self.qvm_collection

    def __exit__(self, type, value, traceback):
        self.qvm_collection.unlock_db()

def main():
    unittest.main()

if __name__ == '__main__':
    main()
