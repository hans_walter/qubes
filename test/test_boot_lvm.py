import test_base

class TestBootFromLVM(test_base.TestBase):

    def test_confg(self):
        with test_base.Lock(self.qvm) as qvm:
            source_template = qvm.get_vm_by_name('fedora-20-x64')

        kw = {'verbose': True, 'source_template': source_template}
        self.b = self.vm.create_storage
        self.vm.create_storage = self.f
        self.assertTrue(self._cod(**kw))
        with test_base.Lock(self.qvm) as qvm:
            qvm.save()


    def f(self, a):
        self.assertTrue(a.hooks_get_config_params)
        a.hooks_get_config_params.rootdev = '/dev/datengrab/fedora-20-x64_20140505022948' 
        a.hooks_get_config_params.privatedev = '/dev/datengrab/fedora-20_home_-x64_20140505022948'
