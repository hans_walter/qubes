import test_base
import unittest
class TestCreateOnDisk(test_base.TestBase):
    """ This tests the create on disc function of a QubesVmNG.

        This should be able to handle the following cases:
        - TemplateVM is an image file
        - TemplateVM is an thin lvm snapshot
    """

    def tearDown(self):
        try:
            os.rmdir('/var/lib/qubes/vm-templates/' + self.name)
        except(OSError):
            None

    def test_create_on_disk_no_template(self):
        kw = {'verbose': True, 'source_template': None,
            'vg_name':self.vg_name, 'pool': self.pool }
        with self.assertRaises(AssertionError):
            self._cod(**kw)

    def test_create_on_disk_src_template_file(self):
        with test_base.Lock(self.qvm) as qvm:
            source_template = qvm.get_vm_by_name('fedora-20-x64')
        kw = {'verbose': True, 'source_template': source_template,
            'vg_name':self.vg_name, 'pool': self.pool }

        self.assertTrue(self._cod(**kw))



